package edu.hneu.mjt.voloshanenko.ticket_1_13.model;

import lombok.Data;
import jakarta.persistence.*;

@Entity
@Data
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String genre;
    private double budget;

    @ManyToOne
    @JoinColumn(name = "director_id", nullable = false)
    private Director director;
}

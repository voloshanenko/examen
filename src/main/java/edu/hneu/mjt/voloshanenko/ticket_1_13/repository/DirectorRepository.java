package edu.hneu.mjt.voloshanenko.ticket_1_13.repository;

import edu.hneu.mjt.voloshanenko.ticket_1_13.model.Director;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DirectorRepository extends JpaRepository<Director, Long> {
}

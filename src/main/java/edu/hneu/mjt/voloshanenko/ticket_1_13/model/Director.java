package edu.hneu.mjt.voloshanenko.ticket_1_13.model;

import lombok.Data;
import jakarta.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
@Data
public class Director {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String genres;
    private int numberOfMovies;
    private String awards;

    @Pattern(regexp = "\\+380\\(\\d{2}\\)\\d{3}-\\д{2}-\\д{2}")
    private String phoneNumber;
}

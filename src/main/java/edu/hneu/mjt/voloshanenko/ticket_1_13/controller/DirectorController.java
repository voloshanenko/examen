package edu.hneu.mjt.voloshanenko.ticket_1_13.controller;

import edu.hneu.mjt.voloshanenko.ticket_1_13.dto.DirectorDTO;
import edu.hneu.mjt.voloshanenko.ticket_1_13.dto.FilmDTO;
import edu.hneu.mjt.voloshanenko.ticket_1_13.service.DirectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/directors")
public class DirectorController {

    @Autowired
    private DirectorService directorService;

    @GetMapping
    public List<DirectorDTO> getAllDirectors() {
        return directorService.getAllDirectors();
    }

    @PostMapping
    public DirectorDTO createDirector(@RequestBody DirectorDTO directorDTO) {
        return directorService.saveDirector(directorDTO);
    }

    @PutMapping("/{id}")
    public DirectorDTO updateDirector(@PathVariable Long id, @RequestBody DirectorDTO directorDTO) {
        directorDTO.setId(id);
        return directorService.updateDirector(directorDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteDirector(@PathVariable Long id) {
        directorService.deleteDirector(id);
    }

    @GetMapping("/{directorId}/films")
    public List<FilmDTO> getFilmsByDirector(@PathVariable Long directorId) {
        return directorService.getFilmsByDirectorId(directorId);
    }
}

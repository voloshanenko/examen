package edu.hneu.mjt.voloshanenko.ticket_1_13.service;

import edu.hneu.mjt.voloshanenko.ticket_1_13.dto.FilmDTO;
import edu.hneu.mjt.voloshanenko.ticket_1_13.model.Film;
import edu.hneu.mjt.voloshanenko.ticket_1_13.repository.FilmRepository;
import edu.hneu.mjt.voloshanenko.ticket_1_13.repository.DirectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FilmService {
    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private DirectorRepository directorRepository;

    public List<FilmDTO> getAllFilms() {
        return filmRepository.findAll().stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    public FilmDTO saveFilm(FilmDTO filmDTO) {
        Film film = convertToEntity(filmDTO);
        Film savedFilm = filmRepository.save(film);
        return convertToDTO(savedFilm);
    }

    public FilmDTO updateFilm(FilmDTO filmDTO) {
        Film film = convertToEntity(filmDTO);
        Film updatedFilm = filmRepository.save(film);
        return convertToDTO(updatedFilm);
    }

    public void deleteFilm(Long id) {
        filmRepository.deleteById(id);
    }

    private FilmDTO convertToDTO(Film film) {
        FilmDTO dto = new FilmDTO();
        dto.setId(film.getId());
        dto.setTitle(film.getTitle());
        dto.setGenre(film.getGenre());
        dto.setBudget(film.getBudget());
        dto.setDirectorId(film.getDirector().getId());
        return dto;
    }

    private Film convertToEntity(FilmDTO dto) {
        Film film = new Film();
        film.setId(dto.getId());
        film.setTitle(dto.getTitle());
        film.setGenre(dto.getGenre());
        film.setBudget(dto.getBudget());
        film.setDirector(directorRepository.findById(dto.getDirectorId()).orElseThrow());
        return film;
    }
}

package edu.hneu.mjt.voloshanenko.ticket_1_13.repository;

import edu.hneu.mjt.voloshanenko.ticket_1_13.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilmRepository extends JpaRepository<Film, Long> {
    List<Film> findByDirectorId(Long directorId);
}

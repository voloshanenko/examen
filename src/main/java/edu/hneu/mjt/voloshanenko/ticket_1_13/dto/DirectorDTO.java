package edu.hneu.mjt.voloshanenko.ticket_1_13.dto;

import lombok.Data;

@Data
public class DirectorDTO {
    private Long id;
    private String name;
    private String genres;
    private int numberOfMovies;
    private String awards;
    private String phoneNumber;
}

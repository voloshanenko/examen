package edu.hneu.mjt.voloshanenko.ticket_1_13.dto;

import lombok.Data;

@Data
public class FilmDTO {
    private Long id;
    private String title;
    private String genre;
    private double budget;
    private Long directorId;
}

package edu.hneu.mjt.voloshanenko.ticket_1_13.controller;

import edu.hneu.mjt.voloshanenko.ticket_1_13.dto.FilmDTO;
import edu.hneu.mjt.voloshanenko.ticket_1_13.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/films")
public class FilmController {
    @Autowired
    private FilmService filmService;

    @GetMapping
    public List<FilmDTO> getAllFilms() {
        return filmService.getAllFilms();
    }

    @PostMapping
    public FilmDTO createFilm(@RequestBody FilmDTO filmDTO) {
        return filmService.saveFilm(filmDTO);
    }

    @PutMapping("/{id}")
    public FilmDTO updateFilm(@PathVariable Long id, @RequestBody FilmDTO filmDTO) {
        filmDTO.setId(id);
        return filmService.updateFilm(filmDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteFilm(@PathVariable Long id) {
        filmService.deleteFilm(id);
    }
}

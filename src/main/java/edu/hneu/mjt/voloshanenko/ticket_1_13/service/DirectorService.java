package edu.hneu.mjt.voloshanenko.ticket_1_13.service;

import edu.hneu.mjt.voloshanenko.ticket_1_13.dto.DirectorDTO;
import edu.hneu.mjt.voloshanenko.ticket_1_13.dto.FilmDTO;
import edu.hneu.mjt.voloshanenko.ticket_1_13.model.Director;
import edu.hneu.mjt.voloshanenko.ticket_1_13.model.Film;
import edu.hneu.mjt.voloshanenko.ticket_1_13.repository.DirectorRepository;
import edu.hneu.mjt.voloshanenko.ticket_1_13.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DirectorService {

    @Autowired
    private DirectorRepository directorRepository;

    @Autowired
    private FilmRepository filmRepository;

    public List<DirectorDTO> getAllDirectors() {
        return directorRepository.findAll().stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    public DirectorDTO saveDirector(DirectorDTO directorDTO) {
        Director director = convertToEntity(directorDTO);
        Director savedDirector = directorRepository.save(director);
        return convertToDTO(savedDirector);
    }

    public DirectorDTO updateDirector(DirectorDTO directorDTO) {
        Director director = convertToEntity(directorDTO);
        Director updatedDirector = directorRepository.save(director);
        return convertToDTO(updatedDirector);
    }

    public void deleteDirector(Long id) {
        directorRepository.deleteById(id);
    }

    public List<FilmDTO> getFilmsByDirectorId(Long directorId) {
        return filmRepository.findByDirectorId(directorId).stream()
                .map(this::convertToFilmDTO)
                .collect(Collectors.toList());
    }

    private DirectorDTO convertToDTO(Director director) {
        DirectorDTO dto = new DirectorDTO();
        dto.setId(director.getId());
        dto.setName(director.getName());
        dto.setGenres(director.getGenres());
        dto.setNumberOfMovies(director.getNumberOfMovies());
        dto.setAwards(director.getAwards());
        dto.setPhoneNumber(director.getPhoneNumber());
        return dto;
    }

    private Director convertToEntity(DirectorDTO dto) {
        Director director = new Director();
        director.setId(dto.getId());
        director.setName(dto.getName());
        director.setGenres(dto.getGenres());
        director.setNumberOfMovies(dto.getNumberOfMovies());
        director.setAwards(dto.getAwards());
        director.setPhoneNumber(dto.getPhoneNumber());
        return director;
    }

    private FilmDTO convertToFilmDTO(Film film) {
        FilmDTO dto = new FilmDTO();
        dto.setId(film.getId());
        dto.setTitle(film.getTitle());
        dto.setGenre(film.getGenre());
        dto.setBudget(film.getBudget());
        dto.setDirectorId(film.getDirector().getId());
        return dto;
    }
}
